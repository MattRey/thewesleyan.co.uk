{{--
  Template Name: Holding Page Template
--}}

@extends('layouts.holding-page')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-page')
  @endwhile
@endsection
