{{--
  Title: History Block
  Description: History block
  Category: formatting
  Icon: admin-comments
  Keywords: example
  Mode: edit
  Align: false
  SupportsAlign: false
  SupportsMode: true
  SupportsMultiple: true
--}}

{{--Add the @use decorator to pull in the class methods (and, by extension, the BaseBlock class methods)--}}
@use(\App\Blocks\HistoryBlock)

<div id="history" class="block {{ $block['classes'] }}">
  <div class="solid-one" data-aos="fade-left" data-aos-anchor-placement="top-center">
    <div class="background-image" style="background-image: url('{{ HistoryBlock::getBackgroundImage()["url"] }}');">&nbsp;</div>
  </div>
  <div class="solid-two" data-aos="fade-left" data-aos-delay="100" data-aos-anchor-placement="top-center">&nbsp;</div>
  <div class="container inner-block">
    <div class="row">
      <div class="col-12 col-lg-8 offset-lg-4 title" data-aos="fade-right" data-aos-delay="200" data-aos-anchor-placement="top-center">
        <h1>{!! HistoryBlock::getTitle() !!}</h1>
        <hr />
      </div>
      <div class="col-lg-6 order-2 order-lg-1">
        <div class="image-block" data-aos="fade-right" data-aos-delay="300" data-aos-anchor-placement="top-bottom" style="background-image: url('{{ HistoryBlock::getImage()["url"] }}')">&nbsp;
          <div class="disclaimer-text" data-aos="fade-left" data-aos-delay="500" data-aos-anchor-placement="top-bottom">
            <span>{!! HistoryBlock::getDisclaimer() !!}</span>
          </div>
        </div>
      </div>
      <div class="col-lg-6 order-1 order-lg-2"  data-aos="fade-up" data-aos-delay="400" data-aos-anchor-placement="top-center">

        <div class="content row">
          <div class="col-md-6">
            {!! HistoryBlock::getContent() !!}
          </div>
          <div class="col-md-6">
            {!! HistoryBlock::getContentTwo() !!}
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
