{{--
  Title: Location Block
  Description: Location block
  Category: formatting
  Icon: admin-comments
  Keywords: example
  Mode: edit
  Align: false
  SupportsAlign: false
  SupportsMode: true
  SupportsMultiple: true
--}}

{{--Add the @use decorator to pull in the class methods (and, by extension, the BaseBlock class methods)--}}
@use(\App\Blocks\LocationBlock)

<div id="location" class="block {{ $block['classes'] }}">
  <div class="solid-one" data-aos="fade-left">
    <div class="background-image" style="background-image: url('{{ LocationBlock::getBackgroundImage()["url"] }}');">&nbsp;</div>
  </div>
  <div class="container inner-block">
    <div class="row">
      <div class="col-12 col-lg-8 offset-lg-4 title" data-aos="fade-right" data-aos-delay="200">
        <h1>{!! LocationBlock::getTitle() !!}</h1>
        <hr />
      </div>
      <div class="col-lg-6 order-2 order-lg-1">
        <div class="image-block" data-aos="fade-down" data-aos-delay="300" data-aos-anchor-placement="top-bottom" style="background-image: url('{{ LocationBlock::getImage()["url"] }}')">&nbsp;</div>
        <div class="locations" data-aos="fade-left" data-aos-delay="500" data-aos-anchor-placement="top-bottom">
          <h6>{!! LocationBlock::getLocationTitle() !!}</h6>
          <div class="location-table">
          @foreach(LocationBlock::getLocations() as $location)
            <h6>{!! $location["city"] !!}</h6>
            <span>{!! $location["time_by_train"] !!}</span>
            <span>{!! $location["time_by_car"] !!}</span>
            @endforeach
          </div>
        </div>
      </div>
      <div class="col-lg-6 order-1 order-lg-2" style="position: relative" data-aos="fade-up" data-aos-delay="400" data-aos-anchor-placement="top-center">
        <div class="content row">
          <div class="col-12 subtitle">
            <h2>{!! LocationBlock::getSubtitle() !!}</h2>
          </div>
        </div>
        <div class="row solid-two">
          <div class="col-md-6">
            {!! LocationBlock::getContent() !!}
          </div>
          <div class="col-md-6">
            {!! LocationBlock::getContentTwo() !!}
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
