{{--
  Title: Intro Block
  Description: Intro block
  Category: formatting
  Icon: admin-comments
  Keywords: example
  Mode: edit
  Align: false
  SupportsAlign: false
  SupportsMode: true
  SupportsMultiple: true
--}}

{{--Add the @use decorator to pull in the class methods (and, by extension, the BaseBlock class methods)--}}
@use(\App\Blocks\IntroBlock)

<div id="intro" class="block {{ $block['classes'] }}">
  <div class="solid-one" data-aos="fade-left">&nbsp;</div>
  <div class="solid-two" data-aos="fade-left">&nbsp;</div>
  <div class="container inner-block">
    <div class="row align-items-center">
      <div class="col-lg-6" data-aos="fade-down" data-aos-delay="300">
        <hr />
        <h1>{!! IntroBlock::getTitle() !!}</h1>
        <hr />
        <h5>{!! IntroBlock::getSubtitle() !!}</h5>
        {!! IntroBlock::getContent() !!}
        @if(IntroBlock::getCtaLink() === 'email')
          <div class="cta">
            <a href="mailto:{!! get_field('email', 'options') !!}" target="_blank"><button>Email us now</button></a>
          </div>
        @else
          <div class="cta">
            <a href="tel:{!! get_field('contact-number', 'options') !!}" target="_blank"><button>Call us now</button></a>
          </div>
        @endif
        <hr />
      </div>
      <div class="col-lg-6">
        <div class="image-block" data-aos="fade-up" data-aos-delay="600" style="background-image: url('{{ IntroBlock::getImage()["url"] }}')">
          <div class="disclaimer-text" data-aos="fade-left" data-aos-delay="600">
            <span>{!! IntroBlock::getDisclaimer() !!}</span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
