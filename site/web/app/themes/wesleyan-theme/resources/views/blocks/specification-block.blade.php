{{--
  Title: Specification Block
  Description: Specification block
  Category: formatting
  Icon: admin-comments
  Keywords: example
  Mode: edit
  Align: false
  SupportsAlign: false
  SupportsMode: true
  SupportsMultiple: true
--}}

{{--Add the @use decorator to pull in the class methods (and, by extension, the BaseBlock class methods)--}}
@use(\App\Blocks\SpecificationBlock)

<div id="specification" class="block {{ $block['classes'] }}">
  <div class="solid-one" data-aos="fade-left">
    <div class="container-fluid inner-block">
      <div class="row">
        <div class="col-lg-6 order-2 order-lg-1 left-col" data-aos="fade-right" data-aos-delay="100"
             data-aos-anchor-placement="top-bottom">
          <div class="image-block" style="background-image: url('{{ SpecificationBlock::getImage()["url"] }}')">&nbsp;
            <div class="disclaimer-text" data-aos="fade-left" data-aos-delay="500" data-aos-anchor-placement="top-bottom">
              <span>{!! SpecificationBlock::getDisclaimer() !!}</span>
            </div>
          </div>
        </div>
        <div class="col-lg-6 order-1 order-lg-2">
          <div class="content row">
            <div class="col-12 title" data-aos="fade-right" data-aos-delay="200" data-aos-anchor-placement="top-bottom">
              <h1>{!! SpecificationBlock::getTitle() !!}</h1>
              <hr/>
            </div>
          </div>
          <div class="row solid-two">
            <div class="col-12 spec-block">
              @foreach(SpecificationBlock::getSpecs() as $spec)
                <div class="individual-spec">
                  <a class="spec-link" data-toggle="collapse" href="#{{$spec["heading"]}}" role="button"
                     aria-expanded="false" aria-controls="collapseExample">
                    {{$spec["heading"]}}
                    <span>+</span>
                  </a>
                  <div class="collapse" id="{{$spec["heading"]}}">
                    {!! $spec["content"] !!}
                  </div>
                </div>
              @endforeach
            </div>
            {{--          <div class="col-md-6">--}}
            {{--            {!! SpecificationBlock::getContentTwo() !!}--}}
            {{--          </div>--}}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
