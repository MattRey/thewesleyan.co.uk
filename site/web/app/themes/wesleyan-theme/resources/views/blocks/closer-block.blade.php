{{--
  Title: Closer Block
  Description: Closer Look block
  Category: formatting
  Icon: admin-comments
  Keywords: example
  Mode: edit
  Align: false
  SupportsAlign: false
  SupportsMode: true
  SupportsMultiple: true
--}}

{{--Add the @use decorator to pull in the class methods (and, by extension, the BaseBlock class methods)--}}
@use(\App\Blocks\CloserBlock)

<div id="closer-look" class="block {{ $block['classes'] }}">
  <div class="solid-one" data-aos="fade-left">
    <div class="container-fluid inner-block">
      <div class="row">
        <div class="col-lg-6 order-2 order-lg-1 left-col" data-aos="fade-right" data-aos-delay="100"
             data-aos-anchor-placement="top-center">
          <div class="image-block" style="background-image: url('{{ CloserBlock::getImage()["url"] }}')">&nbsp;
            <div class="disclaimer-text" data-aos="fade-left" data-aos-delay="500"
                 data-aos-anchor-placement="top-bottom">
              <span>{!! CloserBlock::getDisclaimer() !!}</span>
            </div>
          </div>
        </div>
        <div class="col-lg-6 order-1 order-lg-2">
          <div class="content row">
            <div class="col-12 title" data-aos="fade-left" data-aos-delay="100">
              <h1>{!! CloserBlock::getTitle() !!}</h1>
              <hr/>
            </div>
            <div class="col-sm-8 offset-sm-2 subtitle" data-aos="fade-right" data-aos-delay="200">
              <h6>{!! CloserBlock::getSubtitle() !!}</h6>
            </div>
          </div>
          <div class="row solid-two" data-aos="fade-up" data-aos-delay="400">
            <div class="col-md-6">
              {!! CloserBlock::getContent() !!}
            </div>
            <div class="col-md-6">
              {!! CloserBlock::getContentTwo() !!}
            </div>
          </div>
          <div class="row floorplan-images">
            <div class="col-12 col-md-4 floorplan-image-block" data-aos="fade-down" data-aos-delay="50"
                 data-aos-anchor-placement="top-bottom"
                 style="background-image: url('{{ CloserBlock::getFloorplanOne()["url"] }}')">&nbsp;
            </div>
            <div class="col-12 col-md-4 floorplan-image-block" data-aos="fade-down" data-aos-delay="150"
                 data-aos-anchor-placement="top-bottom"
                 style="background-image: url('{{ CloserBlock::getFloorplanTwo()["url"] }}')">&nbsp;
            </div>
            <div class="col-12 col-md-4 floorplan-image-block" data-aos="fade-down" data-aos-delay="250"
                 data-aos-anchor-placement="top-bottom"
                 style="background-image: url('{{ CloserBlock::getFloorplanThree()["url"] }}')">&nbsp;
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
