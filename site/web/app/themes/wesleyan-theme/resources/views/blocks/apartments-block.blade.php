{{--
Title: Apartments Block
Description: Apartments block
Category: formatting
Icon: admin-comments
Keywords: example
Mode: edit
Align: false
SupportsAlign: false
SupportsMode: true
SupportsMultiple: true
--}}

{{--Add the @use decorator to pull in the class methods (and, by extension, the BaseBlock class methods)--}}
<!--@use(\App\Blocks\ApartmentsBlock)-->

<div id="apartments" class="block {{ $block['classes'] }}">
  <div class="solid-one" data-aos="fade-left">&nbsp;</div>
  <div class="solid-two" data-aos="fade-left" data-aos-delay="200">&nbsp;</div>
  <div class="container-fluid inner-block">
    @include('partials.slider', array('data' => App\Controllers\Slider::getApartments()->posts))
  </div>
</div>
