{{--
  Title: Contact Block
  Description: Contact block
  Category: formatting
  Icon: admin-comments
  Keywords: example
  Mode: edit
  Align: false
  SupportsAlign: false
  SupportsMode: true
  SupportsMultiple: true
--}}

{{--Add the @use decorator to pull in the class methods (and, by extension, the BaseBlock class methods)--}}
@use(\App\Blocks\ContactBlock)

<div id="contact" class="block {{ $block['classes'] }}">
  <div class="solid-one" data-aos="fade-left">&nbsp;</div>
  <div class="solid-two" data-aos="fade-left" data-aos-delay="200">&nbsp;</div>
  <div class="container inner-block">
    <div class="row">
      <div class="form-block col-md-8 offset-md-4" data-aos="fade-left" >
        <h1>{!! ContactBlock::getTitle() !!}</h1>

        [ninja_form id=1]
      </div>
    </div>
  </div>
</div>
