<header class="site-header">
  <div class="container">
    <div class="row d-flex header-row flex-nowrap justify-content-between align-items-center">
      <div class="col-auto flex-shrink-0 navbar-brand">
        @php
          if ( function_exists( 'the_custom_logo' ) ) {
            the_custom_logo();
          }
        @endphp
      </div>
      <div class="col-auto position-static" style="padding-right: 0">
{{--        <nav class="nav-primary d-none d-md-block">--}}
{{--          @if (has_nav_menu('primary_navigation'))--}}
{{--            {!! wp_nav_menu($primarymenu) !!}--}}
{{--          @endif--}}
{{--        </nav>--}}
        <nav class="nav-mobile">
          <div id="navToggler" class="toggle-nav" aria-label="Toggle navigation" aria-expanded="false" aria-controls="expanded-nav">
            <div class="hamburger">&nbsp;</div>
            <div class="hamburger">&nbsp;</div>
            <div class="hamburger">&nbsp;</div>
          </div>
          <div class="expanded-nav" id="expanded-nav">
            <div class="container d-flex flex-column align-items-center">
              @if (has_nav_menu('mobile_navigation'))
                {!! wp_nav_menu($mobilemenu) !!}
              @endif
            </div>
            <div class="nav-bottom">
              <div class="footer-logo" style="background-color: white; width: 100%; height: 100%; mask-image: url('@php echo esc_url( wp_get_attachment_url( get_theme_mod( 'custom_logo' ) ) ); @endphp'); -webkit-mask-image: url('@php echo esc_url( wp_get_attachment_url( get_theme_mod( 'custom_logo' ) ) ); @endphp'); -webkit-mask-repeat: no-repeat; -webkit-mask-position: 50% 50%; -webkit-mask-size: 30%">&nbsp;</div>
            </div>
          </div>
        </nav>
      </div>

    </div>
  </div>
</header>

