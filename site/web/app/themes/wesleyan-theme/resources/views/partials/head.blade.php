<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
{{--  check for IE browser--}}
  <script type='text/javascript'>
    function isItIE() {
      const user_agent = navigator.userAgent;
      const is_it_ie = user_agent.indexOf("MSIE ") > -1 || user_agent.indexOf("Trident/") > -1;
      return is_it_ie;
    }
    if (isItIE()){
      alert(`The browser you are using is no longer supported.\nInternet Explorer is no longer maintained and you should download an alternative now to view this site.`);
    }
  </script>
  @php wp_head() @endphp
</head>
