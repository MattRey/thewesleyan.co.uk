<footer class="content-info h-100">
  <div class="container d-flex flex-column justify-content-between h-100">
{{--    @php dynamic_sidebar('sidebar-footer') @endphp--}}
    <div class="row">
      <div class="col-md-6 col-lg-3">
        <h5>Company address:</h5>
        <p>{!! get_field('address', 'options') !!}</p>
      </div>
      <div class="col-md-6 col-lg-3">
        <h5>Contact details:</h5>
        <div class="contact-small d-flex align-items-center">
          <img src="{{ @\App\asset_path('images/icons/phone_icon.png') }}" alt="">
          <a href="tel:{!! get_field('company-number', 'options') !!}">{!! get_field('company-number', 'options') !!}</a>
        </div>
        <div class="contact-small d-flex align-items-center">
          <img src="{{ @\App\asset_path('images/icons/email_icon.png') }}" alt="">
          <a href="mailto:{!! get_field('company-email', 'options') !!}">{!! get_field('company-email', 'options') !!}</a>
        </div>
      </div>
      <div class="col-md-6 col-lg-3">
        <h5>About us:</h5>
        {!! get_field('companies', 'options') !!}
      </div>
      <div class="col-md-6 col-lg-3">
        <div class="footer-logo" style="background-color: #8a633e; width: 100%; height: 80px; margin-top: 30px; mask-image: url('@php echo esc_url( wp_get_attachment_url( get_theme_mod( 'custom_logo' ) ) ); @endphp'); -webkit-mask-image: url('@php echo esc_url( wp_get_attachment_url( get_theme_mod( 'custom_logo' ) ) ); @endphp'); -webkit-mask-repeat: no-repeat; -webkit-mask-position: center top; -webkit-mask-size: 80%">&nbsp;</div>
        {!! get_field('final-block', 'options') !!}
      </div>
    </div>
    <div class="row">
      <div class="col-12 d-flex justify-content-center">
        <h6>
          © {{ wp_date('Y') }} {!! get_field('copyright-text', 'options') !!}
        </h6>
      </div>
    </div>
  </div>
</footer>
