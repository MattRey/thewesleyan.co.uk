{{--
  Use like: @include('partials.slider', array('data' => App\Controllers\Slider::getApartments()->posts))
  where the controller function passed to $data returns a WP_Query object
--}}
@if (empty($data))
  <div class="alert">
    {{ __('Sorry, no apartment posts are available for the slider.', 'sage') }}
  </div>
@else
  <div id="carouselApartmentsControls" class="carousel slide" data-interval="false" data-ride="carousel">
    <div class="carousel-inner">
      @php $i = 1; @endphp
      @foreach($data as $item)
        @php $apartment = Apartment::getApartments($item->ID) @endphp
        <div class="carousel-item @if($i == 1) active @endif">
          <div class="row">
            <div class="col-12 col-lg-6 offset-lg-6 title" data-aos="fade-right" data-aos-anchor-placement="top-center">
              <h1>{!! get_the_title($item->ID) !!}</h1>
              <hr/>
            </div>
            <div class="col-lg-6 left-col" data-aos="fade-right"
                 data-aos-anchor-placement="top-center">
              <div class="image-block" style="background-image: url({{ get_the_post_thumbnail_url($item->ID) }})">
                <div class="disclaimer-text" data-aos="fade-left" data-aos-delay="500"
                     data-aos-anchor-placement="top-bottom">
                  <span>Computer generated image of {!! get_the_title($item->ID) !!}</span>
                </div>
              </div>
            </div>
            <div class="col-lg-6 right-col" data-aos="fade-up"
                 data-aos-anchor-placement="top-center">
              @php $num = "first" @endphp
              @foreach($apartment as $single)
                <div class="content row">
                  <div class="col-lg-6 apartment-data">
                    <div class="row">
                      <div class="col-6 details">
                        <h6>{!! $single["name"] !!}</h6>
                        <p>{!! $single["summary"] !!}</p>
                        <p>
                          <strong>TOTAL AREA:</strong><br/>
                          {!! $single["area"] !!}
                        </p>
                      </div>
                      @if($single["position"])
                      @php
                      $pos_image = $single["position"]["url"]
                      @endphp
                      @endif
                      <div class="col-6 plan-image"
                           style="background-image: url('{{ $pos_image }}');">&nbsp;
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-12">
                        <div class="dimensions">
                          @if($single["dimensions"])
                            <ol>
                              @foreach($single["dimensions"] as $dimension)
                                <li>
                                  <div>
                                    <span>{!! $dimension["room"] !!}</span><span>{!! $dimension["metric"] !!}</span><span>{!! $dimension["imperial"] !!}</span>
                                  </div>
                                </li>
                              @endforeach
                            </ol>
                          @endif
                        </div>
                      </div>
                    </div>
                  </div>

                  @if ($single["floorplan"])
                    @php $plan_image = $single["floorplan"]["url"] @endphp
                  @endif
                  <div class="col-lg-6 floorplan-holder">
                    <div class="floorplan-image {{$num}}"
                         style="background-image: url('{{ $plan_image }}');">&nbsp;
                    </div>
                  </div>
                </div>
                @php $num = "second" @endphp
              @endforeach
              <div class="dimension-disclaimer">
                {!! get_field('disclaimer', 'options') !!}
              </div>
            </div>
          </div>
        </div>
        @php $i ++; @endphp
      @endforeach
      <a class="carousel-control-prev" href="#carouselApartmentsControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselApartmentsControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </div>
@endif
