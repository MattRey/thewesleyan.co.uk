<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp style="background-image: url({{@\App\asset_path('images/building.png')}});">
    <div class="wrap container" role="document">
      <div class="content">
        <main class="main">
          <img src="{{@\App\asset_path('images/wesleyan_logo.png')}}" alt="">
          @yield('content')
        </main>
      </div>
    </div>
    @php do_action('get_footer') @endphp
    @include('partials.footer')
    @php wp_footer() @endphp
  </body>
</html>
