export default {
  init() {
    jQuery('.site-header').toArray().forEach((header) => {
      const $header = jQuery(header);

      const $toggleNav = $header.find('.nav-mobile .toggle-nav');

      $toggleNav.on('click', () => {
        const shouldExpand = $toggleNav.attr('aria-expanded') !== 'true';

        $header.toggleClass('nav-expanded', shouldExpand);

        $toggleNav.attr('aria-expanded', shouldExpand ? 'true' : 'false');
      });
    });

    const navLinks = document.querySelectorAll('.nav-link');
    const navToggle = document.getElementsByClassName('toggle-nav')[0];
    navLinks.forEach((item) => {
      item.addEventListener('click', () => {
        console.log(navToggle)
        navToggle.setAttribute('aria-expanded', 'false')
      })
    })
  },
};
