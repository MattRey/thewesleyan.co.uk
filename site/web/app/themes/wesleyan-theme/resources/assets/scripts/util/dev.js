import wpEnv from '../util/env';
/**
 * debugging actions for development
 *
 * dev is an class which contains methods that only action in a "development" environment
 */
class Dev {
  /**
   * dev.log - console.log in development environment
   * @param {string} message string to be console.logged
   */
  log(message) {
    if (wpEnv('development')) {
      message && console.log(message)
    }
    return;
  }
}
const dev = new Dev()
export default dev;
