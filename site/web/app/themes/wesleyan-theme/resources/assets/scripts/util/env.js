/**
 * Check if passed environment is current env
 *
 * @param {string} env The environment type to check, e.g. development, staging, production
 *
 * @return {bool}
 */
export default (env) => {
  if (typeof wpSiteInfo === 'undefined' && typeof wpSiteInfo.environment === 'undefined') {
    return false;
  }
  if (env === wpSiteInfo.environment) {
    return true;
  }
  return false;
}
