import mobileNav from '../util/mobileNav';
import AOS from 'aos';

export default {
  init() {
    // JavaScript to be fired on all pages

    //add animations to gutenberg gallery images
    const currentGallery = document.querySelectorAll('.blocks-gallery-item')
    currentGallery.forEach((item) => {
      item.setAttribute('data-aos', 'fade-up')
      item.setAttribute('data-aos-anchor-placement', 'top-bottom')
    })

    mobileNav.init();
    AOS.init({
      easing: 'ease-out',
      duration: '600',
    });

    // Back to top button
    (function() {
      'use strict';

      function trackScroll() {
        const scrolled = window.pageYOffset;
        const coords = document.documentElement.clientHeight;

        if (scrolled > coords) {
          goTopBtn.classList.add('back_to_top-show');
        }
        if (scrolled < coords) {
          goTopBtn.classList.remove('back_to_top-show');
        }
      }

      function backToTop() {
        if (window.pageYOffset > 0) {
          window.scrollTo({ top: 0, behavior: 'smooth' });
          setTimeout(backToTop, 0);
        }
      }

      const goTopBtn = document.querySelector('.back_to_top');

      window.addEventListener('scroll', trackScroll);
      goTopBtn.addEventListener('click', backToTop);
    })();
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
