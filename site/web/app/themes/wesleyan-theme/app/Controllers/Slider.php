<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class Slider extends Controller
{
    public function __construct() {

    }

    // Function to fetch all the person posts that are part of the cast taxonomy
    public static function getApartments() {
        /** @var array The args to pass into WP_Query. */
        $query_args = [
            'post_type' => 'apartments',
            'post_status' => 'publish',
            'posts_per_page' => '-1',
            'order' => 'ASC',        // oldest first
        ];

        /** @var \WP_Query Get the Sector posts from people (as tax) terms. */
        $post_query = new \WP_Query($query_args);

        return $post_query;
    }
}
