<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class App extends Controller
{
    public function siteName()
    {
        return get_bloginfo('name');
    }

    /**
     * Primary Nav Menu arguments
     * @return array
     */
    public function primarymenu() {
        return array(
            'theme_location' => 'primary_navigation',
            'depth' => 1,
            'fallback_cb' => false,
            'item_spacing' => 'discard',
            'container_class' => 'nav',
            'walker' => new \App\wp_bootstrap4_navwalker(),
        );
    }

    /**
     * Mobile Nav Menu arguments
     * @return array
     */
    public function mobilemenu() {
        return array(
            'theme_location' => 'mobile_navigation',
            'depth' => 1,
            'fallback_cb' => false,
            'item_spacing' => 'discard',
            'container_class' => 'menu-container',
            'walker' => new \App\wp_bootstrap4_navwalker(),
        );
    }


    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title();
    }
}
