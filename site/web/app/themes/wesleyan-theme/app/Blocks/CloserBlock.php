<?php

namespace App\Blocks;
use \StoutLogic\AcfBuilder\FieldsBuilder;

$block = new FieldsBuilder('closer-block');

$block

    ->addText('title', [
        'label' => 'Title field',
    ])
    ->addText('subtitle', [
        'label' => 'Subtitle field',
    ])
    ->addWysiwyg('content', [
        'label' => 'Content Column 1'
    ])
    ->addWysiwyg('content-two', [
        'label' => 'Content Column 2'
    ])
    ->addImage('image', [
        'label' => 'Main image'
    ])
    ->addText('disclaimer', [
        'label' => 'Disclaimer text'
    ])
    ->addImage('floorplan-one', [
        'label' => 'Floorplan one'
    ])
    ->addImage('floorplan-two', [
        'label' => 'Floorplan two'
    ])
    ->addImage('floorplan-three', [
        'label' => 'Floorplan three'
    ])

    ->setLocation('block', '==', 'acf/closer-block');

add_action('acf/init', function() use ($block) {
    acf_add_local_field_group($block->build());
});

/**
 * Class CloserBlock
 * @package App\Blocks
 * Add a class with the same name as your block file that extends BaseBlock
 */

class CloserBlock extends BaseBlock
{
    /**
     * Define any further unique class methods here,
     * for use within the individual block
     */

    public static function getContentTwo() {
        return get_field('content-two');
    }

    public static function getFloorplanOne() {
        return get_field('floorplan-one');
    }

    public static function getFloorplanTwo() {
        return get_field('floorplan-two');
    }

    public static function getFloorplanThree() {
        return get_field('floorplan-three');
    }

};
