<?php

namespace App\Blocks;
use \StoutLogic\AcfBuilder\FieldsBuilder;

$block = new FieldsBuilder('intro-block');

$block

    ->addText('title', [
        'label' => 'Title field',
    ])
    ->addText('subtitle', [
        'label' => 'Subtitle field',
    ])
    ->addWysiwyg('content', [
        'label' => 'Content'
    ])
    ->addImage('image', [
        'label' => 'Image'
    ])
    ->addText('disclaimer', [
        'label' => 'Disclaimer text'
    ])
    ->addSelect('button-action', [
        'label' => 'Connect action button to email or phone',
        'choices' => [
            'email' => 'Email',
            'phone' => 'Telephone'
        ],
        'default_value' => ['email'],
        'ui' => 1
    ])

    ->setLocation('block', '==', 'acf/intro-block');

add_action('acf/init', function() use ($block) {
    acf_add_local_field_group($block->build());
});

/**
 * Class IntroBlock
 * @package App\Blocks
 * Add a class with the same name as your block file that extends BaseBlock
 */

class IntroBlock extends BaseBlock
{
    /**
     * Define any further unique class methods here,
     * for use within the individual block
     */
    public static function getCtaLink() {
        return get_field('button-action');
    }

};
