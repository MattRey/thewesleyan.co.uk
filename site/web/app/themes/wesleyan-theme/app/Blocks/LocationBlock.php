<?php

namespace App\Blocks;
use \StoutLogic\AcfBuilder\FieldsBuilder;

$block = new FieldsBuilder('location-block');

$block

    ->addText('title', [
        'label' => 'Title field',
    ])
    ->addText('subtitle', [
        'label' => 'Subtitle field',
    ])
    ->addImage('background', [
        'label' => 'Background image',
    ])
    ->addWysiwyg('content', [
        'label' => 'Content Column 1'
    ])
    ->addWysiwyg('content-two', [
        'label' => 'Content Column 2'
    ])
    ->addImage('image', [
        'label' => 'Image'
    ])
    ->addText('location-title', [
        'label' => 'Location block title'
    ])
    ->addRepeater('locations', [
        'label' => 'Convenient location points'
    ])
        ->addText('city')
        ->addText('time_by_train')
        ->addText('time_by_car')
    ->endRepeater()

    ->setLocation('block', '==', 'acf/location-block');

add_action('acf/init', function() use ($block) {
    acf_add_local_field_group($block->build());
});

/**
 * Class LocationBlock
 * @package App\Blocks
 * Add a class with the same name as your block file that extends BaseBlock
 */

class LocationBlock extends BaseBlock
{
    /**
     * Define any further unique class methods here,
     * for use within the individual block
     */
    public static function getBackgroundImage() {
        return get_field("background");
    }

    public static function getContentTwo() {
        return get_field("content-two");
    }

    public static function getLocationTitle() {
        return get_field('location-title');
    }

    public static function getLocations() {
        return get_field('locations');
    }

};
