<?php

namespace App\Blocks;
use \StoutLogic\AcfBuilder\FieldsBuilder;

/**
 * Class BaseBlock
 * @package App\Blocks
 * A base class containing frequently used methods for blocks.
 * Add to the methods when you encounter a frequently used method that can be re-used within blade files for our blocks.
 * To use these methods:
 * 1. extend your block class to use BaseBlock (e.g. class ExampleBlock extends BaseBlock { ....) in your block php file
 * 2. add the @use(\App\Blocks\ExampleBlock) decorator at the top of your view (replacing ExampleBlock with the name of your newly constructed block class)
 * 3. Call the methods in your blade file using the static method type e.g {{ ExampleBlock::title() }}
 */

class BaseBlock
{

    public static function getTitle() {
        $upper_case_title = strtoupper(get_field('title'));
        return $upper_case_title;
    }

    public static function getSubtitle() {
        $upper_case_subtitle = strtoupper(get_field('subtitle'));
        return $upper_case_subtitle;
    }

    public static function getContent() {
        return get_field('content');
    }

    public static function getImage() {
        return get_field('image');
    }

    public static function getDisclaimer() {
        return get_field('disclaimer');
    }

};
