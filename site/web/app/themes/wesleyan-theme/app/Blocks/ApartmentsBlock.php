<?php

namespace App\Blocks;
use \StoutLogic\AcfBuilder\FieldsBuilder;

$block = new FieldsBuilder('apartments-block');

$block

    ->addMessage('message', 'This block displays a slider of the apartments - no options are needed to display the content of this block.')
    ->addText('title', [
        'label' => 'Title field',
    ])

    ->setLocation('block', '==', 'acf/apartments-block');

add_action('acf/init', function() use ($block) {
    acf_add_local_field_group($block->build());
});

/**
 * Class ApartmentsBlock
 * @package App\Blocks
 * Add a class with the same name as your block file that extends BaseBlock
 */

class ApartmentsBlock extends BaseBlock
{
    /**
     * Define any further unique class methods here,
     * for use within the individual block
     */

};
