<?php

namespace App\Blocks;
use \StoutLogic\AcfBuilder\FieldsBuilder;

$block = new FieldsBuilder('history-block');

$block

    ->addText('title', [
        'label' => 'Title field',
    ])
    ->addImage('background', [
        'label' => 'Background image',
    ])
    ->addWysiwyg('content', [
        'label' => 'Content Column 1'
    ])
    ->addWysiwyg('content-two', [
        'label' => 'Content Column 2'
    ])
    ->addImage('image', [
        'label' => 'Image'
    ])
    ->addText('disclaimer', [
        'label' => 'Disclaimer text'
    ])

    ->setLocation('block', '==', 'acf/history-block');

add_action('acf/init', function() use ($block) {
    acf_add_local_field_group($block->build());
});

/**
 * Class HistoryBlock
 * @package App\Blocks
 * Add a class with the same name as your block file that extends BaseBlock
 */

class HistoryBlock extends BaseBlock
{
    /**
     * Define any further unique class methods here,
     * for use within the individual block
     */
    public static function getBackgroundImage() {
        return get_field("background");
    }

    public static function getContentTwo() {
        return get_field("content-two");
    }

};
