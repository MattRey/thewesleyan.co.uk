<?php

namespace App\Blocks;
use \StoutLogic\AcfBuilder\FieldsBuilder;

$block = new FieldsBuilder('contact-block');

$block

    ->addText('title', [
        'label' => 'Title field',
    ])



    ->setLocation('block', '==', 'acf/contact-block');

add_action('acf/init', function() use ($block) {
    acf_add_local_field_group($block->build());
});

/**
 * Class ContactBlock
 * @package App\Blocks
 * Add a class with the same name as your block file that extends BaseBlock
 */

class ContactBlock extends BaseBlock
{
    /**
     * Define any further unique class methods here,
     * for use within the individual block
     */


};
