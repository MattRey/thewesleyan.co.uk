<?php

namespace App\Blocks;
use \StoutLogic\AcfBuilder\FieldsBuilder;

$block = new FieldsBuilder('specification-block');

$block

    ->addText('title', [
        'label' => 'Title field',
    ])
    ->addRepeater('specs', [
        'label' => 'Specification list'
    ])
        ->addText('heading')
        ->addWysiwyg('content')
    ->endRepeater()
//    ->addWysiwyg('content', [
//        'label' => 'Content Column 1'
//    ])
//    ->addWysiwyg('content-two', [
//        'label' => 'Content Column 2'
//    ])
    ->addImage('image', [
        'label' => 'Main image'
    ])
    ->addText('disclaimer', [
        'label' => 'Disclaimer text'
    ])

    ->setLocation('block', '==', 'acf/specification-block');

add_action('acf/init', function() use ($block) {
    acf_add_local_field_group($block->build());
});

/**
 * Class SpecificationBlock
 * @package App\Blocks
 * Add a class with the same name as your block file that extends BaseBlock
 */

class SpecificationBlock extends BaseBlock
{
    /**
     * Define any further unique class methods here,
     * for use within the individual block
     */

    public static function getContentTwo() {
        return get_field('content-two');
    }

    public static function getSpecs() {
        return get_field('specs');
    }

};
