<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

$apartment_options = new FieldsBuilder('apartment_options');

$apartment_options

    ->addTextArea('disclaimer', [
        'label' => 'Disclaimer text for the apartments dimensions'
    ])

    ->setLocation('options_page', '==', 'acf-options-apartment-options');

\add_action('acf/init', function() use ($apartment_options) {
    acf_add_local_field_group($apartment_options->build());
});
