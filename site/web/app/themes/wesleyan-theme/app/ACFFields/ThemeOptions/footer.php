<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

$footer_options = new FieldsBuilder('footer_options');

$footer_options

    ->addText('copyright-text', [
        'label' => 'Copyright text',
    ])
    ->addWysiwyg('companies', [
        'label' => 'Content for the Regenesis/Cheshire Lamont section of the footer'
    ])
    ->addWysiwyg('final-block', [
        'label' => 'Content for the final section of the footer, under the logo.'
    ])

    ->setLocation('options_page', '==', 'acf-options-footer-options');

\add_action('acf/init', function() use ($footer_options) {
    acf_add_local_field_group($footer_options->build());
});
