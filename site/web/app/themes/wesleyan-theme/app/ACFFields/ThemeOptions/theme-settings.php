<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

$theme_options = new FieldsBuilder('theme_options');

$theme_options

    ->addText('disclaimer', [
        'label' => 'Disclaimer text'
    ])

    ->setLocation('options_page', '==', 'theme-general-settings');

\add_action('acf/init', function() use ($theme_options) {
    acf_add_local_field_group($theme_options->build());
});
