<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

$contact_options = new FieldsBuilder('contact_options');

$contact_options

    ->addImage('company-logo')
    ->addTextarea('address', [
        'label' => 'Address',
        'new_lines' => 'br'
    ])
    ->addText('company-number', [
        'label' => 'Contact telephone number for Regenesis.'
    ])
    ->addEmail('company-email', [
        'label' => 'Contact email address for Regenesis.'
    ])
    ->addText('contact-number', [
        'label' => 'Contact telephone number'
    ])
    ->addEmail('email', [
        'label' => 'Contact email address'
    ])

    ->setLocation('options_page', '==', 'acf-options-contact-options');

\add_action('acf/init', function() use ($contact_options) {
    acf_add_local_field_group($contact_options->build());
});
