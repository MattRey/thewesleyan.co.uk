<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

$apartment_options = new FieldsBuilder('apartment_options');
$apartment_options
    ->addRepeater('apartments', [
        'label' => 'Add an apartment.',
        'layout' => 'row',
    ])
        ->addText('name', [
            'label' => 'Apartment name',
        ])
        ->addTextArea('summary', [
            'label' => 'Apartment summary',
            'new_lines' => 'br',
        ])
        ->addText('area', [
            'label' => 'Total Area',
        ])
        ->addImage('position', [
            'label' => 'Position image'
        ])
        ->addImage('floorplan', [
            'label' => 'Floorplan image'
        ])
        ->addRepeater('dimensions', [
            'label', 'Add room dimensions',
        ])
            ->addText('room')
            ->addText('metric')
            ->addText('imperial')
        ->endRepeater()

    ->endRepeater()


    ->setLocation('post_type', '==', 'apartments');

\add_action('acf/init', function() use ($apartment_options) {
    acf_add_local_field_group($apartment_options->build());
});

class Apartment
{
    public static function getApartments($post_id) {
        return get_field('apartments', $post_id);
    }
};
