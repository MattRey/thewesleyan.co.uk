<?php

namespace App\Blocks;

include_once 'Blocks/BaseBlock.php';
include_once 'Blocks/IntroBlock.php';
include_once 'Blocks/HistoryBlock.php';
include_once 'Blocks/LocationBlock.php';
include_once 'Blocks/CloserBlock.php';
include_once 'Blocks/SpecificationBlock.php';
include_once 'Blocks/ContactBlock.php';
include_once 'Blocks/ApartmentsBlock.php';
